import UIKit

class Singleton {
    // 饿汉式
    var val1 = "饿汉式加载"
    lazy var val2 = "懒汉式"
    var val3: String?
    
    func showVal1() {
        print(val1)
    }
    func showVal2() {
        print(val2)
    }
    func showVal3() {
        if val3 == nil {
            val3 = "懒汉式"
        }
        print(val3!)
    }
}

var singleton = Singleton()
singleton.showVal1()
singleton.showVal2()
singleton.showVal3()

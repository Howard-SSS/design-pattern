import UIKit
// 抽象原型
protocol Prototype {
    func clone() -> Prototype
}
// 具体原型
class ConcretePrototype: Prototype {
    var val1: Int
    var val2: String
    
    init(val1: Int, val2: String) {
        self.val1 = val1
        self.val2 = val2
    }
    func clone() -> Prototype {
        return ConcretePrototype(val1: val1, val2: val2)
    }
}
// 具体原型
class ConcretePrototype1: NSCopying, Equatable {
    var val1: Int
    var val2: String
    
    init(val1: Int, val2: String) {
        self.val1 = val1
        self.val2 = val2
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        return ConcretePrototype1(val1: val1, val2: val2)
    }
    
    static func == (lhs: ConcretePrototype1, rhs: ConcretePrototype1) -> Bool {
        return lhs.val1 == rhs.val1 && lhs.val2 == rhs.val2
    }
}


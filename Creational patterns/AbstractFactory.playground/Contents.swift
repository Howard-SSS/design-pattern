import UIKit
// 抽象产品A
protocol AbstractProductA {
    func create()
}
// 具体产品A1
class AbstractProductA1: AbstractProductA{
    func create() {
        print("生产长袖")
    }
}
// 具体产品A2
class AbstractProductA2: AbstractProductA{
    func create() {
        print("生产短袖")
    }
}
// 抽象产品B
protocol AbstractProductB {
    func create()
}
// 具体产品B1
class AbstractProductB1: AbstractProductB{
    func create() {
        print("生产长裤")
    }
}
// 具体产品B2
class AbstractProductB2: AbstractProductB{
    func create() {
        print("生产短裤")
    }
}
// 抽象工厂
protocol AbstractFactory {
    func createProductA() -> AbstractProductA
    func createProductB() -> AbstractProductB
}
// 具体工厂（冬季工厂）
class ConcreateFactory1: AbstractFactory {
    func createProductA() -> AbstractProductA {
        return AbstractProductA1()
    }
    func createProductB() -> AbstractProductB {
        return AbstractProductB1()
    }
}
// 具体工厂（夏季工厂）
class ConcreateFactory2: AbstractFactory {
    func createProductA() -> AbstractProductA {
        return AbstractProductA2()
    }
    func createProductB() -> AbstractProductB {
        return AbstractProductB2()
    }
}
var abstractFactory: AbstractFactory!
print("""
    ---
    现在冬季
    ---
    """)
abstractFactory = ConcreateFactory1()
abstractFactory.createProductA().create()
abstractFactory.createProductB().create()
print("""
    ---
    现在夏季
    ---
    """)
// 换厂
abstractFactory = ConcreateFactory2()
abstractFactory.createProductA().create()
abstractFactory.createProductB().create()

import UIKit
// 抽象建造者
protocol Builder {
    func buildPartA()
    func buildPartB()
}
// 具体产品
class Product {
    private var result = [String]()
    func add(part: String){
        result.append(part)
    }
    func listPath() -> String {
        return result.joined(separator: ",") + "\n"
    }
}
// 具体建造者
class ConcreteBuilder: Builder {
    private var product = Product()
    func buildPartA() {
        product.add(part: "pathA")
    }
    func buildPartB() {
        product.add(part: "pathB")
    }
    func getProduct() -> Product {
        let product = self.product
        self.product = Product()
        return product
    }
}
// 指挥者
class Director {
    var builder: Builder
    init(builder: Builder) {
        self.builder = builder
    }
    func planA() {
        builder.buildPartA()
    }
    func planB() {
        builder.buildPartA()
        builder.buildPartB()
    }
}
let build = ConcreteBuilder()
var director = Director(builder: build)
director.planA()
print(build.getProduct().listPath())
director.planB()
print(build.getProduct().listPath())

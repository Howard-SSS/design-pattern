import UIKit

// 抽象工厂
protocol Creator {
    func createProduct() -> Product
}
// 具体工厂A
class ConcreteCreatorA: Creator {
    func createProduct() -> Product {
        return ConcreteProductA()
    }
}
// 具体工厂B
class ConcreteCreatorB: Creator {
    func createProduct() -> Product {
        return ConcreteProductB()
    }
}
// 抽象产品
protocol Product {
    func Show()
}
// 具体产品A
class ConcreteProductA: Product {
    func Show() {
        print("this is A")
    }
}
// 具体产品B
class ConcreteProductB: Product {
    func Show() {
        print("this is B")
    }
}

var creator: Creator?
var product: Product?
creator = ConcreteCreatorA()
product = creator?.createProduct()
product?.Show()
// 换工厂
creator = ConcreteCreatorB()
product = creator?.createProduct()
product?.Show()

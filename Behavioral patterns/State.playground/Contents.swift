import Foundation
// 这个结构可能看上去与策略模式相似， 但有一个关键性的不同——在状态模式中， 特定状态知道其他所有状态的存在， 且能触发从一个状态到另一个状态的转换； 策略则几乎完全不知道其他策略的存在。
class Context {
    
    private var status: Status
    
    init(status: Status) {
        self.status = status
        transitionTo(status: status)
    }
    func operation() {
        status.operation()
    }
    func transitionTo(status: Status) {
        self.status = status
        self.status.update(context: self)
    }
}
protocol Status {
    func update(context: Context)
    func operation()
}
class BaseStatus: Status {
    
    private(set) weak var context: Context?
    
    func update(context: Context) {
        self.context = context
    }
    func operation() { }
}
class RedLight: BaseStatus {
    override func operation() {
        print("现在红灯,等5秒后变绿灯")
        sleep(5)
        context?.transitionTo(status: GreenLight())
    }
}
class GreenLight: BaseStatus {
    override func operation() {
        print("现在绿灯,等10秒后变黄灯")
        sleep(10)
        context?.transitionTo(status: YellowLight())
    }
}
class YellowLight: BaseStatus {
    override func operation() {
        print("现在黄灯,等1秒后变红灯")
        sleep(1)
        context?.transitionTo(status: RedLight())
    }
}
let context = Context(status: RedLight())
while(true) {
    context.operation()
}

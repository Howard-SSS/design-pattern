import UIKit
// 抽象主题
protocol Subject {
    func registerObserver(observer: Observer)
    func unregisterObserver(observer: Observer)
    func notifyObserver()
}
// 抽象观察者角色
@objc protocol Observer {
    func update()
}
// 具体主题角色
class ConcreteSubject: Subject {
    var userList = [Observer]()
    func registerObserver(observer: Observer) {
        userList.append(observer)
    }
    func unregisterObserver(observer: Observer) {
        let index = userList.firstIndex(where: { obs in
            obs === observer
        })!
        userList.remove(at: index)
    }
    func notifyObserver() {
        for observer in userList {
            observer.update()
        }
    }
}
// 具体观察者
class ConcreteObserver1: Observer {
    var subject: Subject
    init(subject: Subject) {
        self.subject = subject
    }
    func update() {
        print("观察者1开始做饭")
    }
}
// 具体观察者
class ConcreteObserver2: Observer {
    var subject: Subject
    init(subject: Subject) {
        self.subject = subject
    }
    func update() {
        print("观察者2开始切菜")
    }
}

var subject = ConcreteSubject()
var obs1 = ConcreteObserver1(subject: subject),obs2 = ConcreteObserver2(subject: subject)
subject.registerObserver(observer: obs1)
subject.registerObserver(observer: obs2)
subject.notifyObserver()

import UIKit
// 各策略并不知道其他策略,只能上下文决定使用策略并切换
// 环境角色
class Context {
    private var strategy: Strategy
    init(strategy: Strategy) {
        self.strategy = strategy
    }
    func setStrategy(strategy: Strategy) {
        self.strategy = strategy
    }
    func lookAlgorithm() {
        strategy.algorithm()
    }
}
// 抽象策略
protocol Strategy {
    func algorithm()
}
// 具体策略
class ConcreteStrategyA: Strategy {
    func algorithm() {
        print("折线图展示")
    }
}
// 具体策略
class ConcreteStrategyB: Strategy {
    func algorithm() {
        print("柱状图展示")
    }
}
var strategyA = ConcreteStrategyA(),strategyB = ConcreteStrategyB()
var context = Context(strategy: strategyA)
context.lookAlgorithm()
context.setStrategy(strategy: strategyB)
print("转换")
context.lookAlgorithm()

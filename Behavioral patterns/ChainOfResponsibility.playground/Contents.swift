import UIKit
// 处理者
protocol Handler {
    var handler: Handler? {get}
    func setNext(handler: Handler)
    func getNext() -> Handler?
    func handlerRequest(num: Int)
}
// 具体处理者
class ConcreteHandlerA: Handler {
    var handler: Handler?
    
    func setNext(handler: Handler) {
        self.handler = handler
    }
    
    func getNext() -> Handler? {
        return handler
    }
    
    func handlerRequest(num: Int) {
        if num <= 1 {
            print("班主任批转请假\(num)天")
        } else {
            print("班主任最多批准1天，还需其他领导批准")
            getNext()?.handlerRequest(num: num)
        }
    }
}
// 具体处理者
class ConcreteHandlerB: Handler {
    var handler: Handler?
    
    func setNext(handler: Handler) {
        self.handler = handler
    }
    
    func getNext() -> Handler? {
        return handler
    }
    
    func handlerRequest(num: Int) {
        if num <= 7 {
            print("辅导员批转请假\(num)天")
        } else {
            print("辅导员最多批准7天，还需其他领导批准")
            getNext()?.handlerRequest(num: num)
        }
    }
}
// 具体处理者
class ConcreteHandlerC: Handler {
    var handler: Handler?
    
    func setNext(handler: Handler) {
        self.handler = handler
    }
    
    func getNext() -> Handler? {
        return handler
    }
    
    func handlerRequest(num: Int) {
        print("校长批准请假")
    }
}
var con1 = ConcreteHandlerA(), con2 = ConcreteHandlerB(), con3 = ConcreteHandlerC()
con1.setNext(handler: con2)
con2.setNext(handler: con3)
let num = 40
print("学生请假\(num)天")
con1.handlerRequest(num: num)

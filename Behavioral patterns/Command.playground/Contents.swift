// Context -> Invoker -> Command -> Receiver
// 命令层 屏蔽实现细节
protocol Command {
    func execute()
}
class CommandPrint: Command {
    
    private var parameter: String

    init(parameter: String) {
        self.parameter = parameter
    }
    
    func execute() {
        print("打印\(parameter)")
    }
}
class CommandCopy: Command {
    
    private var parameter: String
    
    init(parameter: String) {
        self.parameter = parameter
    }
    
    func execute() {
        print("复制\(parameter)")
    }
}
//封装参数进行更多复杂操作
//class ComplexCommand: Command {
//
//    private var receiver: Receiver
//
//    private var parameter: String
//
//    init(receiver: Receiver, parameter: String) {
//        self.receiver = receiver
//        self.parameter = parameter
//    }
//
//    func execute() {
//        receiver.method(parameter)
//    }
//}
class Invoker {
    
    var command: Command?
    
    func doSomething() {
        command?.execute()
    }
}
let invoker = Invoker()
invoker.command = CommandPrint(parameter: "广告")
invoker.doSomething()
invoker.command = CommandCopy(parameter: "台词")
invoker.doSomething()

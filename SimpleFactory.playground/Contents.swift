import UIKit

protocol Product {
    func show()
}

class ConcreteProductA: Product {
    func show() {
        Swift.print("This is A")
    }
}

class ConcreteProductB: Product {
    func show() {
        Swift.print("This is B")
    }
}

class SimpleProductFactory {
    static func createProduct(type: String) -> Product?{
        if type == "A" || type == "a"{
            return ConcreteProductA()
        } else if type == "B" || type == "b" {
            return ConcreteProductB()
        }
        return nil
    }
}

var product: Product?
product = SimpleProductFactory.createProduct(type: "A")
product?.show()
product = SimpleProductFactory.createProduct(type: "B")
product?.show()

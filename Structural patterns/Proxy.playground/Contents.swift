import UIKit
// 抽象角色
protocol Abstraction {
    func operation()
}
// 修正抽象化角色
class RefinedAbstraction: Abstraction {
    func operation() {
        print("A操作")
    }
}
// 实现化角色
protocol Implementor {
    var abstraction: Abstraction { get set }
    func operationImp()
}
// 具体实现化角色
class ConcreteImplementor: Implementor {
    var abstraction: Abstraction
    init(abstraction: Abstraction) {
        self.abstraction = abstraction
    }
    func operationImp() {
        print("B代理")
        abstraction.operation()
    }
}
var abstraction: Abstraction = RefinedAbstraction()
var implementor: Implementor = ConcreteImplementor(abstraction: abstraction)
implementor.operationImp()

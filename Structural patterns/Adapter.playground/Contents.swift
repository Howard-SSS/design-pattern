import UIKit
// 目标
protocol Target {
    func request()
}
// 源
class Adaptee {
    func specificRequest() {
        print("旧方法")
    }
}
// MARK: - 类适配器
// 适配器
class Adapter1: Adaptee, Target {
    func request() {
        print("新增修改")
        super.specificRequest()
        print("新增修改")
    }
}

// MARK: - 对象适配器
// 适配器
class Adapter2: Target {
    var adaptee: Adaptee
    init(adaptee: Adaptee) {
        self.adaptee = adaptee
    }
    func request() {
        print("新增修改")
        adaptee.specificRequest()
        print("新增修改")
    }
}

var adaptee = Adaptee()
var adapter1 = Adapter1()
var adapter2 = Adapter2(adaptee: adaptee)
adapter1.request()
adapter2.request()

import UIKit
// MARK: - 单纯享元模式
// 抽象享元
protocol Flyweight {
    func operation(externState: String)
}
// 具体享元
class ConcreteFlyweight: Flyweight {
    let intrinsicState: String
    init(intrinsicState: String) {
        self.intrinsicState = intrinsicState
    }
    func operation(externState: String) {
        print("颜色\(intrinsicState),位置\(externState)")
    }
}
// 抽象享元
class FlyweightFactory {
    var map =  Dictionary<String, Flyweight>()
    func setFlyweight(key: String, value: Flyweight) {
        map[key] = value
    }
    // 获取单纯享元对象
    func getFlyweight(key: String) -> Flyweight {
        return map[key]!
    }
}

let black = ConcreteFlyweight(intrinsicState: "black"),white = ConcreteFlyweight(intrinsicState: "white")
let flyweightFactory = FlyweightFactory()
flyweightFactory.setFlyweight(key: "black", value: black)
flyweightFactory.setFlyweight(key: "white", value: white)
print("开始对弈")
flyweightFactory.getFlyweight(key: "black").operation(externState: "(4,3)")
flyweightFactory.getFlyweight(key: "white").operation(externState: "(8,8)")
flyweightFactory.getFlyweight(key: "black").operation(externState: "(4,4)")
flyweightFactory.getFlyweight(key: "white").operation(externState: "(6,8)")

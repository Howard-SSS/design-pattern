import UIKit
// 抽象构件
protocol Component {
    
    func getDescription() -> String
}
// 具体构件
class ConcreteComponent: Component {
    var str: String = "建了栋房子"
    
    func getDescription() -> String {
        return str
    }
}
// 抽象装饰者
protocol Decorator: Component {
    var component : Component! {get set}
}
// 具体装饰者
class ConcreteDecorator1: Decorator {
    var component: Component!
    
    func getDescription() -> String {
        return component.getDescription() + "，加了张桌子"
    }
}
// 具体装饰者
class ConcreteDecorator2: Decorator {
    var component: Component!
    
    func getDescription() -> String {
        return component.getDescription() + "，加了张椅子"
    }
}

let component = ConcreteComponent()
let con1 = ConcreteDecorator1()
con1.component = component
let con2 = ConcreteDecorator2()
con2.component = con1
print(con2.getDescription())
